// Author Tiago Neves
// 05-06/2019

//This class need Jquery to work

var smsconfirm = {
        host:'',
        fieldPhoneNumber:'', // to get Jquery Object
        fieldToCodeSMS:'', // to get Jquery Object
        countingWait:'', // to get Jquery Object
        callback2nothing:null, // function after counting end
        callback2fail:null, // function to call if error appear
        callback2ok:null, // function if all right!
        waittime:60,
        hash:'',
        startProtocol: function() {
            if (window.jQuery == undefined) { 
                alert("jQuery is needed to SMS Confirm");
                return false;
            }
            $.ajax({
                url:smsconfirm.host,
                type: 'get',
                dataType:'json',
                crossDomain: true,
                data:'type=phone&n=' + $(smsconfirm.fieldPhoneNumber).val(),
                success: function(data,status,jx) {
                    if (status=='success') {
                        smsconfirm.counting();
                        smsconfirm.hash = data.hash;
                        $(smsconfirm.fieldPhoneNumber).parent().hide();
                        $(smsconfirm.fieldToCodeSMS).keyup(function(){
                            if ($(smsconfirm.fieldToCodeSMS).val().lenght<5) { return false; } 
                            smsconfirm.sendCode();
                        });
                        $(smsconfirm.fieldToCodeSMS).parent().show();
                    } else {    
                        console.warn(jx);
                        alert('There is a problem when send phonenumber to host. See console log.');
                    }
                },
                error: function(jx,status,xthrow) {
                    console.warn(status, xthrow, jx);
                    alert('There is a problem when send phonenumber to host. See console log.');
                }
            });
    },
    counting: function() {  
        var inter = setInterval(function(){
            $(smsconfirm.countingWait).html(smsconfirm.waittime);
            smsconfirm.waittime--;
            if (smsconfirm.waittime==0) {
                smsconfirm.callback2nothing();
                clearInterval(inter);
            }
        }, 1000);
    },
    sendCode: function() {
        $.ajax({
            url: smsconfirm.host,
            dataType:'json',
            data: 'type=code&n=' + $(smsconfirm.fieldToCodeSMS).val() 
                + '&hash=' + smsconfirm.hash,
            type: 'get',
            crossDomain: true,
            success: function(data,status,jx) {
                console.log(data.code);
                if (data.code==true) {
                    smsconfirm.callback2ok();
                } else {
                    smsconfirm.callback2fail();
                }
            }
        });
    }
}

    

