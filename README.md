
# SMS Confirmation JS

Smsconfirm is a Object to help identify a Phone.


## Requiriments

To run correctly 'smsconfirm class' your application, you need:

- [ ] A HTML element input text to put phone number visible after run smsConfirmation.startProtocol()
- [ ] A HTML element input text to put code receive SMS HIDDEN after run smsConfirmation.startProtocol()
- [ ] JQuery load


## How This Works

1. Send user phone number to a host (the host is a responsible to create a hash and send SMS)
2. Host must return a hash in response to send number 
3. If app receive hash go to 5 step this list
4. If app not receive the hash, it return a error and register in the console log
5. Hide the parent div of the phone number input element set on the class construction
6. Turn Visibility the parent of input element where user will put code from the SMS
7. Set event on code SMS input element to Send a code to host
8. When you type 5 characters the code will send to host
9. Host compare data
10. If data confirm host return true  run callback2ok
11. if data not confirm host return false run callbackFail


## About Backend (HOST)

In the backend developement you may use any technology.
You need only

- Send a HASH to APP
- Send a SMS to Phone
- And when host receive code and hash from the app, confirm with it exist and if it all right



# License

GPL v3




